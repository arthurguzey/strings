package com.java.str;

public class Conversion {

    public String getStringFromInt(int x){
        String str = String.valueOf(x);
        return  str;
    }

    public String getStringFromDouble(double x){
        String str = String.valueOf(x);
        return  str;
    }


    public int getIntFromString(String str){
        int x = Integer.parseInt(str);
        return  x;
    }

    public double getDoubleFromString(String str){
        double x = Double.parseDouble(str);
        return  x;
    }
}
