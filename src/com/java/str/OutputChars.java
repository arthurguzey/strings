package com.java.str;

public class OutputChars {

    public void outputEnglishCapitalLetter(){
        char letter;
        for (int i=65; i<91; i++)
        {
            letter = (char)i;
            System.out.println(letter);
        }
    }
    public void outputEnglishLetter(){
        char letter;
        for (int i=97; i<123; i++)
        {
            letter = (char)i;
            System.out.println(letter);
        }
    }
    public void outputRussianLetter(){
        char letter;
        for (int i='а'; i<='я'; i++)
        {
            letter = (char)i;
            System.out.println(letter);
        }
    }

    public void outputDigits(){
        char letter;
        for (int i=48; i<58; i++)
        {
            letter = (char)i;
            System.out.println(letter);
        }
    }

    public void outputASCII(){
        char letter;
        for (int i=32; i<127; i++)
        {
            letter = (char)i;
            System.out.println(letter);
        }
    }


}
