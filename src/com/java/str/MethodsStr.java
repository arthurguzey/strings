package com.java.str;

public class MethodsStr {

    public int task1(String str)
    {
        str.trim();
        int minWordLength = Integer.MAX_VALUE;
        String[] words = str.split("[\\s.,]+");
        for(int i = 0; i< words.length; i++){
            if ( minWordLength > words[i].length() )
            {
                minWordLength = words[i].length();
            }
        }
        return minWordLength;
    }



    public String[] task2(String[] words, int lengthWordForReplace){
        if(lengthWordForReplace<3)
        {
            return null;
        }
        for (int i=0; i<words.length; i++)
        {
            words[i].trim();
            if(words[i].length()==lengthWordForReplace){
                words[i] = words[i].substring(0,(words[i].length()-3));
                words[i] = words[i].concat("$$$");
            }
        }

        return words;
    }


    public String task3(String str)
    {
        final String[] punctuationMark = new String[]{".", ",", "!", "?", ":"};
        for (int j = 0; j<punctuationMark.length; j++)
        {
            String plusSpace = punctuationMark[j]+" ";
            str=str.replace(punctuationMark[j],  plusSpace);
            str=str.replace("  ",  " ");
        }
        return str;
    }


    public String task4(String str)
    {
        String result = "";
        for (int i=0; i<str.length();i++){
            String repeatChar = ""+str.charAt(i);
            if(!result.contains(repeatChar))
            {
                result = result.concat(repeatChar);
            }
        }
        return result;
    }

    public int task5(String str){
        String[] words = str.split("[\\s.,?!:-]+");
        return words.length;
    }

    public String task6(String str, int startCut, int endCut){
        String firstPart = str.substring(0, startCut);
        String secondPart = str.substring(endCut);
        str = firstPart+secondPart;
        return str;
    }

    public String task7(String str)
    {
        String result = "";
        for (int i=str.length()-1; i>=0;i--){
            char symbol = str.charAt(i);
            result = result+symbol;
        }
        return result;
    }

    public String task8(String str)
    {
        String[] words = str.split("[\\s.,?!:-]+");
        String lastWord = words[words.length-1];
        String result = str.replace(lastWord, "");
        return result;
    }

}
