package test.java.str;

import com.java.str.MethodsStr;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MethodsStrTest {

    MethodsStr cut = new MethodsStr();
    static Arguments[] task1TestArgs(){
        return new Arguments[]{
                Arguments.arguments("иванов иван, ванович", 4),
                Arguments.arguments("яблоко от яблони", 2),
        };
    }
    @ParameterizedTest
    @MethodSource("task1TestArgs")
    void task1Test(String x, int expected){
        int actual = cut.task1(x);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] task2TestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[] {"abcd", "abc", "ncjf"}, 4, new String[] {"a$$$", "abc", "n$$$"}),
                Arguments.arguments(new String[] {"abcd", "abc", "ncjf"}, 2, null),
                Arguments.arguments(new String[] {"aa", "bbbbbb", "cccc"}, 6, new String[] {"aa", "bbb$$$", "cccc"}),

        };
    }
    @ParameterizedTest
    @MethodSource("task3TestArgs")
    void task2Test(String[] x,int l, String[] expected){
        String[] actual = cut.task2(x, l);
        Assertions.assertArrayEquals(expected, actual);
    }


    static Arguments[] task3TestArgs(){
        return new Arguments[]{
                Arguments.arguments("привет,меня зовут. Артур" , "привет, меня зовут. Артур"),
                Arguments.arguments("abc" , "abc"),
                Arguments.arguments("Привет! Артур,как дела?нормально? " , "Привет! Артур, как дела? нормально? "),
                Arguments.arguments("Привет! Артур,как дела,нормально? " , "Привет! Артур, как дела, нормально? "),


        };
    }
    @ParameterizedTest
    @MethodSource("task3TestArgs")
    void task3Test(String x, String expected){
        String actual = cut.task3(x);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] task4TestArgs(){
        return new Arguments[]{
                Arguments.arguments("aaaaa" , "a"),
                Arguments.arguments("abcdbe" , "abcde"),

        };
    }
    @ParameterizedTest
    @MethodSource("task4TestArgs")
    void task4Test(String x, String expected){
        String actual = cut.task4(x);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] task5TestArgs(){
        return new Arguments[]{
                Arguments.arguments("один два три,четыре" , 4),
                Arguments.arguments("каждое слово важно!" , 3),

        };
    }
    @ParameterizedTest
    @MethodSource("task5TestArgs")
    void task5Test(String x, int expected){
        int actual = cut.task5(x);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] task6TestArgs(){
        return new Arguments[]{
                Arguments.arguments("один два три,четыре", 4, 9, "одинтри,четыре"),
                Arguments.arguments("один два три,четыре", 9, 11, "один два и,четыре"),

        };
    }
    @ParameterizedTest
    @MethodSource("task6TestArgs")
    void task6Test(String x,int start, int end, String expected){
        String actual = cut.task6(x, start, end);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] task7TestArgs(){
        return new Arguments[]{
                Arguments.arguments("один", "нидо"),
                Arguments.arguments("два", "авд"),

        };
    }
    @ParameterizedTest
    @MethodSource("task7TestArgs")
    void task7Test(String x, String expected){
        String actual = cut.task7(x);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] task8TestArgs(){
        return new Arguments[]{
                Arguments.arguments("один два три", "один два "),
                Arguments.arguments("два пять!шесть", "два пять!"),

        };
    }
    @ParameterizedTest
    @MethodSource("task8TestArgs")
    void task8Test(String x, String expected){
        String actual = cut.task8(x);
        Assertions.assertEquals(expected, actual);
    }
}
