package test.java.str;

import com.java.str.Conversion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ConversionTest {

    Conversion cut = new Conversion();



    static Arguments[] getStringFromIntTestArgs(){
        return new Arguments[]{
                Arguments.arguments(25,"25"),
                Arguments.arguments(344,"344"),
        };
    }
    @ParameterizedTest
    @MethodSource("getStringFromIntTestArgs")
    void getStringFromIntTest(int x, String expected){
        String actual = cut.getStringFromInt(x);
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] getStringFromDoubleTestArgs(){
        return new Arguments[]{
                Arguments.arguments(25.44,"25.44"),
                Arguments.arguments(344.0007,"344.0007"),
        };
    }
    @ParameterizedTest
    @MethodSource("getStringFromDoubleTestArgs")
    void getStringFromDoubleTest(double x, String expected){
        String actual = cut.getStringFromDouble(x);
        Assertions.assertEquals(expected, actual);
    }




    static Arguments[] getIntFromStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("25", 25),
                Arguments.arguments("344", 344),
        };
    }
    @ParameterizedTest
    @MethodSource("getIntFromStringTestArgs")
    void getIntFromStringTest(String x, int expected){
        int actual = cut.getIntFromString(x);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] getDoubleFromStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("25.33", 25.33),
                Arguments.arguments("344.005", 344.005),
        };
    }
    @ParameterizedTest
    @MethodSource("getDoubleFromStringTestArgs")
    void getDoubleFromStringTest(String x, double expected){
        double actual = cut.getDoubleFromString(x);
        Assertions.assertEquals(expected, actual);
    }




}
