import com.java.str.OutputChars;

public class Main {
    public static void main(String[] args) {
        OutputChars outputChars = new OutputChars();
        outputChars.outputEnglishCapitalLetter();
        outputChars.outputEnglishLetter();
        outputChars.outputRussianLetter();
        outputChars.outputDigits();
        outputChars.outputASCII();
    }
}
